# Contract Test Tutorial
Henrique Dalcomune - hdalcomune

## Table of Contents
* [Requirements](#requirements)
* [Contract Tests](#contract-tests)
  * [Going deep into the subject](#Going-deep-into-the-subject)
  * [The contract](#The-contract)
  * [The new apps](#The-new-apps)
  * [Provider Details](#Provider-Details)
  * [Client Details](#Client-Details)
  * [Testing the New App](#Testing-the-New-App)
  * [Spring Cloud Contract](#Spring-Cloud-Contract)
  * [Testing The Consumer Side](#Testing-The-Consumer-Side)
* [Exercises](#Exercises)
* [References](#references)

## Requirements

For this tutorial you will need [IntelliJ](https://www.jetbrains.com/idea/), install Lombok plugin, and enable Annotation Processors. 

### Install Lombok Plugin
* Go to File > Settings > Plugins OR IntelliJ IDEA > Preferences
* Click on Browse repositories...
* Search for Lombok Plugin
* Click on Install plugin
* Restart IntelliJ IDEA

### Enable Annotation Processors
* Go to File > Settings > Plugins OR IntelliJ IDEA > Preferences
* Click on Build, Execution, Deployment
* Check the option `Enable Annotation Processing`

## CONTRACT TESTS

So far we created a few functional tests ([in this tutorial](https://bitbucket.org/hdalcomune/api-tutorial)) based on the acceptance criteria defined in this tutorial. Our test approach was a mixed bag, where we set a client to make calls to a provider simulating a real environment but without any UI tests. But what if we could reduce the test complexity by simply testing if the contract, a JSON file describing the possible interaction and responses, is respected between a consumer-provider relation? Definitely, it is a good idea but we'll need to change the context here, we cannot test Trello API this way. Why? Because Trello API is a public provider, we don't know how many consumers it has, there's no way to establish all the contracts this way. We'll need to test our own Task application.

### Going deep into the subject
When we chose to do a contract test we're aiming to facilitate the test execution, we want faster feedback and less complexity to the tests, let's compare a few details about the Functional test approach that we used and a Contract test: 
* Complexity:
When testing a REST application in a real environment we may have a dependency from multiple services, all of them with its own version. All the service dependencies need to be deployed with a compatible version of the service that we're testing. For the contract tests, we just need to make sure 
* Test Data: 
 The functional tests need to create all data from scratch or we need to prepare the data inserting into the DB, this might get more complex if we have a cross-reference between different databases. Also, when testing on an E2E approach we need to make sure the data we're sending will be processed correctly, however, if we mock data we can focus only in the syntax of the data.
* Stability:
E2E test environment will always be more complex than any other test that uses mocked data. If an E2E test fails it doesn't necessarily mean that you found an issue in the system. It can suggest that there's an issue with the environment, that could be a configuration issue, a service that could not be reached or even a specific service that was deployed with the wrong version. This can go worse if you rely on third-party services. 
* Feedback: 
Contract tests will run faster since it doesn't deppends on setting up the whole environments with its dependencies and also that a test using a real environment will take more time to run than running contract tests with mocks and stubs. 

### The contract
The premise of consumer-driven contracts is to have a contract defined between one or more consumers and a provider then the contract is tested against the consumers and the provider. 

![Contract Test Flow](images/contract-test-flow.png)

### The new apps

As mentioned, Trello is a public API and can have multiple consumers, including third party ones. Let's have a new exercise, instead. What if we built Contract tests for both a provider and consumer applications? For that, we'll use our own applications, a provider, and a consumer.  

#### Provider Details
Our provider application is called TaskApp, it is a simple API that creates and retrieves tasks. It uses Spring 2 and an H2 Memory DB, meaning it is only for didactic purposes.

Repository: https://bitbucket.org/hdalcomune/taskapp

The application provides the following endpoints: 

Endpoint: `http://localhost:8080/taskapp/task`
| Info               | Method | Endpoint        | Mandatory Parameters      | Optional Parameters   | 
| -------------      | :----: | :-------------: | :-------------:           |  :-------------:      |
| Create a new task  | POST   | /taskapp/task/  | taskId, name, description | status                |


Endpoint: `http://localhost:8080/taskapp/task/{id}`
| Info               | Method | Endpoint            | Mandatory Parameters    | 
| -------------      | :----: | :-------------:     | :-------------:         | 
| Retrieve a task    | GET    | /taskapp/task/{id}  | taskId                  | 

Repository URL: 

To start the application, clone the repository and from your preferred IDE run the class `TaskApplication`. The application will start in port 8080.

#### Client Details

Our consumer is another application called TaskClient, it uses Spring 2 and Feign Client.

Repository: https://bitbucket.org/hdalcomune/taskclient

The consumer has no UI but can also create and list tasks through the API. The difference is that we're using the port 8090 instead. The endpoints are the same:
* `http://localhost:8090/taskapp/task`
* `http://localhost:8080/taskapp/task/{id}`

Repository URL: 

To start the application, clone the repository and from your preferred IDE run the class `TaskApplication`. The application will start in port 8090.

### Testing the New App

Note: Before moving on take a look at both implementations and start the apps, add some tasks using POSTMAN and move on whenever you're ready. 

If you haven't noticed, either the provider and the consumer doesn't have any tests. At this point, we want to make sure every time we use the TaskClient a new task is created. To start let's create a test for the TaskClient app. We'll use Spring Boot Test modules, which provides some libraries and help us to auto-configure the tests. 

Under `src/test/java/` let's create a new class `TaskClientApplicationTests` and add our first test: 

```java
@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskClientApplicationTests {
  @Autowired private TaskClient taskClient;

  @Test
  public void createTaskCompliesToContract() {
    TaskResponse task = new TaskResponse();
    task.setName("Buy Milk");
    task.setDescription("Check milk price at neighborhood market");
    task.setStatus("In Progress");
    TaskResponse response = taskClient.createTask(task);
    assertThat(response, is(task));
  }
}
```

Let's understand what's happening in the code above. We're setting up the classes with two annotations `@RunWith` and `@SpringBootTest`, but what does that mean? Check below:
* `@RunWith` - The test library of our choice is JUnit, this annotation is from JUnit and it tells that will be testing a Spring app, otherwise the upcoming annotations will be ignored. 
* `@SpringBootTest` - This annotation will start the application context described in our main classes, it will not start a server but it will detect our project configuration and make it available from the test perspective.

Next, we created a test `createTaskCompliesToContract` that asserts if the response we get from the service is the same as our test data. Note that, we're using new annotations, `@Test` and `@Autowired`. Let's understand those two: 
* `@Test` - It simply "tells" JUnit that the method under it should be executed as a test case. 
* `@Autowired` - This comes from (Lombok|LINK here), it simply eliminates the need to declared a Getter/Setter to the variable and also to create a constructor. 

Now, let's run our test. What happened? Probably, your test failed because it couldn't reach `http://localhost:8080/taskapp/task`. This is expected since we depend to have our provider up and running to do the test. Try once more but start TaskApp before running it. The test will pass and will get the execution summary similar to this: 
[image]

We just built and ran a test that checks if our consumer complies to the provider contract. Can you see the issues of this approach? Let's analyze it: 
* First, the test is available only at the consumer side. If the contract changes in the provider code the test we created will fail, worst than that, the consumer will no longer work unless it is fixed to comply to the contract again. 
* If we have other consumers it is likely that the same issue occurs. 
* There are no tests at the provider side.

What is necessary to improve? 
* We need contract tests on both ends, provider, and consumer. 
* The test should be created on the provider side.
* Consumers should rely on the same provider tests to validate the contract.

To make all that possible we'll be using another Spring tool to create the tests. 

### Spring Cloud Contract

Spring Cloud Contract is a project that provides support for Consumer Driven Contracts, the key aspect is that we'll be able to create and publish contract tests. At this point we'll not need to change any of the project dependencies in Gradle files, everything is already configured, but we'll be doing the following: 
* Create a contract test
* Run the test in the provider code
* Publish the contract test
* Run the test in the consumer code

#### The Setup

Both projects are already configured with the correct dependencies, tasks and Gradle plugins. If you need further details, check the `build.gradle` files from both consumer and provider projects. More details available at [Spring Cloud Contract Documentation](https://cloud.spring.io/spring-cloud-contract/single/spring-cloud-contract.html#_how_it_works)

#### Creating the Contract Test

Remember that this time we're creating contract tests, we'll not cover business features on it, we just wanna make sure the contract is respected between different applications.

Spring Cloud Contract uses a DSL to specify the contract, this can be done in Groovy or YAML. We'll be using Groovy, it is not necessary to have any knowledge of the language. We'll be recreating the test we did in the consumer before but this time using the DSL. Go to the provider code and create under `src/test/resources/contracts` a new groovy file named `shouldSaveTask.groovy`. Use the code below: 

```Groovy
import org.springframework.cloud.contract.spec.Contract

Contract.make {
    description("When a POST request is made with a task, the task info is returned")
    request {
        method 'POST'
        url '/taskapp/task'
        body(
            name: "Buy Milk",
            description: "Check milk price at neighborhood market"
        )
        headers {
            contentType(applicationJson())
        }
    }
    response {
        status 201
        body(
            id: 1,
            name: "Buy Milk",
            description: "Check milk price at neighborhood market"
        )
        headers {
            contentType(applicationJson())
        }
    }
}
```

See how we specified the test? We informed in the request the method, endpoint, body, and header. For the response, we specified the expected content type and body. We're not setting up any mocks for now.

Now, to make sure our test runs we need to set up our test class, under `src/test/java` create a new abstract class, `TaskApplicationTests`. This class will not have any tests, just the necessary setup to create our mocks. 

```Java
package com.hdalcomune.taskapp;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import com.jayway.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.context.WebApplicationContext;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = TaskApplication.class)
public abstract class TaskApplicationTests {
  @Autowired
  WebApplicationContext webApplicationContext;

  @MockBean
  private TaskRepository taskRepository;

  @Before
  public void setup() {
    Task savedTask = new Task();
    savedTask.setName("Buy Milk");
    savedTask.setDescription("Check milk price at neighborhood market");
    savedTask.setId(Long.valueOf(1));
    when(taskRepository.save(any(Task.class))).thenReturn(savedTask);
    RestAssuredMockMvc.webAppContextSetup(webApplicationContext);
  }
}
```

We created an abstract class, with the same `@RunWith` and `@SpringBootTest` annotations we've used in the consumer test. The difference is that we're instantiating a WebApplicationContext object that provides the application configuration at test level and used `@Before` annotation to set our mock. Observe that we used Mockito and RestAssured to create our test data setup.

#### Generating Contract Tests

Now, comes the good part. We already configured our mocks and created our contract test using the Groovy DSL. Spring Cloud Contract offers the option to auto-generate the contract tests. The project is already configured to use it, there's a Gradle plugin that's is already in the list of dependencies and it is also specified where the generated tests will be saved, which is under `src/generatedContract`. The configuration below was added to Gradle:
```groovy
//THIS IS ALREADY CONFIGURED IN THE PROJECT
contracts {
    testMode = 'MockMvc'
    baseClassForTests = 'com.avenuecode.taskapp.TaskApplicationTests'
    generatedTestSourcesDir = project.file('src/generatedContract')
}
```

Open the terminal and execute `./gradlew generateContractTests`, this will just generate the tests, check the generatedContract directory to see if the auto-generated tests are there. You'll see that a new class, `ContractVerifierTest.java`, was generated.

To execute the test you can also use the same command but passing the parameter "test, eg: `./gradlew generateContractTests tests`.

The output will be printed in the terminal, you can check whether the test has passed or not. 

### Testing the Consumer Side

#### Exporting the stubs

Now that we created the contract test using Spring Cloud Contract for our Provider code we want to reuse the same test to validate the contract in the consumer side. To do that, we'll export our tests to the MavenLocal repository so we can use it in the consumer code. To do that we need to set group, and version in our provider Gradle file, this is already done. To complete, just run `./gradlew publishToMavenLocal` from the provider directory. You'll see that under `build/stubs` there will be a version of our TaskApp stub that was also published to MavenLocal in your machine. 

#### Using the Stub in the Consumer Code

First, we need to inform the consumer app that we'll use a stub. For that, we'll configure the stubrunner in the application.yml file. Add the following block without changing anything else:
```yaml
stubrunner:
  idsToServiceIds:
    taskapp: taskapp
```

Remember the test that we created for the consumer code before? In the same class we'll use a different annotation, `@AutoConfigureStubRunner`, to include our stub. Inside the test class let's add the annotation as below:

```java
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.NONE)
@AutoConfigureStubRunner(
    ids = "com.avenuecode:taskapp:+:stubs:8080",
    stubsMode = StubRunnerProperties.StubsMode.LOCAL
)
public class TaskClientApplicationTests {

  @Autowired private TaskClient taskClient;

  @Test
  public void createTaskCompliesToContract() {
    TaskResponse task = new TaskResponse();
    task.setName("Buy Milk");
    task.setDescription("Check milk price at neighborhood market");
//    task.setStatus("In Progress");
    TaskResponse response = taskClient.createTask(task);
    assertThat(response, is(task));
  }
}
```

We're adding the group and id of the stub we exported to MavenLocal and making it explicit that we want to execute the test offline, without hitting any real service. 

Now, we can run the test class again, you will notice that both contract test from that we exported and the one we created will be executed. 


## Exercises
Create a new Contract test for retrieving Tasks in the provider code. Make sure you execute in both Provider and Consumer codes. Consider the following endpoint:
Endpoint: `http://localhost:8080/taskapp/task/{id}` and `http://localhost:8090/taskclient/task/{id}`
| Info               | Method | Endpoint            | Mandatory Parameters    | 
| -------------      | :----: | :-------------:     | :-------------:         | 
| Retrieve a task    | GET    | /taskapp/task/{id}  | taskId                  | 

## References

### Consumer Driven Contract
https://martinfowler.com/articles/consumerDrivenContracts.html#EvolvingAServiceAnExample
https://dius.com.au/2016/02/03/microservices-pact/
https://reflectoring.io/7-reasons-for-consumer-driven-contracts/

### Spring Cloud Contract
https://cloud.spring.io/spring-cloud-contract/single/spring-cloud-contract.html#spring-cloud-contract-verifier-intro-three-second-tour-consumer
http://cloud.spring.io/spring-cloud-contract/single/spring-cloud-contract.html#_contract_dsl
https://cloud.spring.io/spring-cloud-contract/single/spring-cloud-contract.html#_how_it_works
https://reflectoring.io/consumer-driven-contract-consumer-spring-cloud-contract/

### Annotations
http://www.baeldung.com/spring-autowire
http://junit.sourceforge.net/javadoc/org/junit/Test.html

### SpringBoot
https://docs.spring.io/spring-boot/docs/current/reference/html/boot-features-testing.html
http://g00glen00b.be/producing-rest-apis-with-spring/
http://g00glen00b.be/consuming-rest-apis-with-spring/

### Feign Client 
https://cloud.spring.io/spring-cloud-netflix/multi/multi_spring-cloud-feign.html
https://reflectoring.io/accessing-spring-data-rest-with-feign/